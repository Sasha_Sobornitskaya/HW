package sample;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller {

    String email;
    String pass1;
    String pass2;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button loginButton;

    @FXML
    private PasswordField repeatedPasswordField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextArea resultField;

    @FXML
    private TextField loginField;

    @FXML
    void initialize() {
        loginButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<javafx.scene.input.MouseEvent>() {
            @Override
            public void handle(javafx.scene.input.MouseEvent mouseEvent) {
                email = loginField.getText();
                pass1 = passwordField.getText();
                pass2 = repeatedPasswordField.getText();

                char[] emailArray = email.toCharArray();
                boolean a = false;
                for (int i = emailArray.length - 1; i >= 0; i--) {
                    if (emailArray[i] == '.') {
                        for (int n = i - 1; n >= 0; n--) {
                            if (emailArray[n] == '@') {
                                if (pass1.equals(pass2)) {
                                    a = true;
                                }
                            }
                        }
                    }
                }
                if (a == true) {
                    resultField.setText("You are successfully logged in.");
                }
                else {
                    resultField.setText("Error");
                }
            }
        });
        assert loginButton != null : "fx:id=\"loginButton\" was not injected: check your FXML file 'EmailView.fxml'.";
        assert repeatedPasswordField != null : "fx:id=\"repeatedPasswordField\" was not injected: check your FXML file 'EmailView.fxml'.";
        assert passwordField != null : "fx:id=\"passwordField\" was not injected: check your FXML file 'EmailView.fxml'.";
        assert resultField != null : "fx:id=\"resultField\" was not injected: check your FXML file 'EmailView.fxml'.";
        assert loginField != null : "fx:id=\"loginField\" was not injected: check your FXML file 'EmailView.fxml'.";
    }

}