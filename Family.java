class Family {
    protected String status;
    protected String name;
    protected String sex;
    protected int age;

    public static void Info(String status, String name, String sex, int age) {

        System.out.println("Name: " + name);
        System.out.println("Sex: " + sex);
        System.out.println("Age: " + age);
        System.out.println("Status: " + status);
        System.out.println();
    }

    public static void main(String[] args) {
        Father.Father();
        Mother.Mother();
        Son1.Son1();
        Son2.Son2();
        Son3.Son3();
        Grandma1.Grandma1();
        Grandpa1.Grandpa1();
        Grandma2.Grandma2();
        Grandpa2.Grandpa2();
    }
}

class Father extends Family {
    public static void Father() {
        Family.Info("Father", "Alexander", "Male", 35);
    }
}

class Mother extends Family {
    public static void Mother() {
        Family.Info("Mother", "Anna", "Female", 32);
    }
}

class Son1 extends Family {
    public static void Son1() {
        Family.Info("Alexander's and Anna's son", "Slava", "Male", 8);
    }
}

class Son2 extends Family {
    public static void Son2() {
        Family.Info("Alexander's and Anna's son", "Vanya", "Male", 5);
    }
}

class Son3 extends Family {
    public static void Son3() {
        Family.Info("Alexander's and Anna's son", "Andrej", "Male", 2);
    }
}

class Grandma1 extends Family {
    public static void Grandma1() {
        Family.Info("Alexander's mother", "Olga", "Female", 72);
    }
}

class Grandpa1 extends Family {
    public static void Grandpa1() {
        Family.Info("Alexander's father", "Pavel", "Male", 72);
    }
}

class Grandma2 extends Family {
    public static void Grandma2() {
        Family.Info("Anna's mother", "Inna", "Female", 64);
    }
}

class Grandpa2 extends Family {
    public static void Grandpa2() {
        Family.Info("Anna's father", "Igor", "Male", 70);
    }
}