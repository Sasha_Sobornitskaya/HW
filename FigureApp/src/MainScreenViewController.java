import Draw.Drawer;
import Figures.Circle;
import Figures.Figure;
import Figures.Rectangle;
import Figures.Triangle;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenViewController<T> {
    private List<Figure> figures = new ArrayList<>();
    private Random random;

    @FXML
    private Canvas canvas;
    int index = 0;


    private void addFigure(Figure figure) {
        figures.add(index, figure);
        index++;
    }

    private Figure createFigure(double x, double y) {
        Figure figure = null;
        switch (random.nextInt(3)) {
            case Figure.CIRCLE_CODE:
                figure = new Circle(random.nextInt(50), x, y, random.nextInt(3), Color.RED);
                break;
            case Figure.RECTANGLE_CODE:
                figure = new Rectangle(x, y, random.nextInt(3), Color.BLUE, random.nextInt(50), random.nextInt(100));
                break;
            case Figure.TRIANGLE_CODE:
                figure = new Triangle(x, y, random.nextInt(3), Color.GREEN, random.nextInt(50));
                break;
            default:
                System.out.println("Unknown figure!");
        }

        return figure;
    }

    private void repaint() {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        Drawer<Figure> drawer = new Drawer<>(figures);
        drawer.draw(canvas.getGraphicsContext2D());
    }

    @FXML
    private void onMouseClicked(MouseEvent mouseEvent) {
        addFigure(createFigure(mouseEvent.getX(), mouseEvent.getY()));
        repaint();
    }
        @FXML
        private ResourceBundle resources;

        @FXML
        private URL location;

        @FXML
        void initialize() {
            assert canvas != null : "fx:id=\"canvas\" was not injected: check your FXML file 'MainScreenView.fxml'.";
        }

}
